import { defineConfig } from 'dumi';
import { metas } from './config/metas';
const repo = 'RuoYi-Cloud-Ant-Design';

export default defineConfig({
  title: repo,
  favicon:
    'https://snkkkait.oss-cn-beijing.aliyuncs.com/picgo/ruoyi-antdesign-4.png',
  logo:
    'https://snkkkait.oss-cn-beijing.aliyuncs.com/picgo/ruoyi-antdesign-4.png',
  outputPath: 'docs-dist',
  mode: 'site',
  hash: true,
  // Because of using GitHub Pages
  base: `/`,
  publicPath: `/`,
  navs: [
    null,
    {
      title: 'GitHub',
      path: 'https://github.com/snkkka163',
    },
  ],
  locales: [['zh-CN', '中文']],
  metas,
  // more config: https://d.umijs.org/config
});
