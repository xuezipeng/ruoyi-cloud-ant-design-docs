---
group:
  title: 基础
nav:
  title: 文档列表
  path: /components
---

## 联系

- 如果您发现了什么bug，或者有什么界面建议或意见；

  欢迎提交 [issue](https://gitee.com/y_project/RuoYi-Cloud/issues)

  `RuoYi Ant Design风格总交流(包含SpringBoot/SpringCloud)`群：

  [![加入QQ群](https://img.shields.io/badge/Q%E7%BE%A4-1038609759-blue.svg)](https://jq.qq.com/?_wv=1027&k=Cq8fZnrj)

  如果讨论后端，推荐添加若依微服务后端交流群（官方）：

  [![加入QQ群](https://img.shields.io/badge/Q%E7%BE%A4-130643120-blue.svg)](https://jq.qq.com/?_wv=1027&k=bg4UX4uf)

