---
order: 1
group:
  title: 基础
nav:
  title: 文档列表
  path: /components
---

## 简介

# RuoYi-Cloud-Antdv
![](https://snkkkait.oss-cn-beijing.aliyuncs.com/picgo/ruoyi-antdesign-4.png)

- 项目介绍
> RuoYi-Cloud-Antdv是若依微服务RuoYi-Cloud版本Ant Design风格前端

- [RuoYi-Vue](https://gitee.com/y_project/RuoYi-Vue) 基于SpringBoot，Spring Security，JWT，Vue 的前后端分离权限管理系统
- [Ant Design Vue](https://github.com/vueComponent/ant-design-vue/) An enterprise-class UI components based on Ant Design and Vue
- [Ant Design Vue Pro](https://github.com/vueComponent/ant-design-vue-pro) Use Ant Design Vue like a Pro
- [vditor](https://github.com/Vanessa219/vditor) 一款浏览器端的 Markdown 编辑

<!-- ![](https://gitee.com/xuezipeng/ruoyi-cloud-ant-design/badge/star.svg?theme=dark) -->
[![](https://gitee.com/xuezipeng/ruoyi-cloud-ant-design/badge/star.svg?theme=dark)](https://gitee.com/xuezipeng/ruoyi-cloud-ant-design)
[![](https://img.shields.io/github/stars/snkkka163/ruoyi-cloud-ant-design.svg?style=social)](https://github.com/snkkka163/ruoyi-cloud-ant-design)
[![](https://img.shields.io/badge/RuoYi_Cloud-2.5.0-brightgreen)](https://gitee.com/y_project/RuoYi-Cloud)
[![](https://img.shields.io/badge/Ant_Design_Vue-1.7.2-brightgreen)](https://www.antdv.com/)
[![](https://img.shields.io/badge/Ant_Design_Vue_Pro-3.0.0-brightgreen)](https://pro.antdv.com/)

- 现有功能
> **在线构建器由于没有合适的开源项目，后期决定等待大佬的开源项目，若无则抽空自实现~**
1. 用户管理：用户是系统操作者，该功能主要完成系统用户配置。
2. 部门管理：配置系统组织机构（公司、部门、小组），树结构展现支持数据权限。
3. 岗位管理：配置系统用户所属担任职务。
4. 菜单管理：配置系统菜单，操作权限，按钮权限标识等。
5. 角色管理：角色菜单权限分配、设置角色按机构进行数据范围权限划分。
6. 字典管理：对系统中经常使用的一些较为固定的数据进行维护。
7. 参数管理：对系统动态配置常用参数。
8. 通知公告：系统通知公告信息发布维护。
9. 操作日志：系统正常操作日志记录和查询；系统异常信息日志记录和查询。
10. 登录日志：系统登录日志记录查询包含登录异常。
11. 在线用户：当前系统中活跃用户状态监控。
12. 定时任务：在线（添加、修改、删除)任务调度包含执行结果日志。
13. 代码生成：前后端代码的生成（java、html、xml、sql）支持CRUD下载 。
14. 系统接口：根据业务代码自动生成相关的api接口文档。
15. 服务监控：监视当前系统CPU、内存、磁盘、堆栈等相关信息。
16. 在线构建器：拖动表单元素生成相应的HTML代码。**(待开发！)**
17. 连接池监视：监视当前系统数据库连接池状态，可进行分析SQL找出系统性能瓶颈。

