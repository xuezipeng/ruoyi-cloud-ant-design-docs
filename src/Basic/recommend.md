---
group:
  title: 基础
nav:
  title: 文档列表
  path: /components
---

## 推荐

- [RuoYi-Vue](https://gitee.com/y_project/RuoYi-Vue) 基于SpringBoot，Spring Security，JWT，Vue 的前后端分离权限管理系统
- [RuoYi-Vue-Ant-Design](https://gitee.com/fuzui/RuoYi-Antdv) 基于RuoYi-Vue(SpringBoot)版本的Ant Design风格前端