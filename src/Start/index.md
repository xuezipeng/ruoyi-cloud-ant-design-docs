---
group:
  title: 快速开始
nav:
  title: 文档列表
  path: /components
---

## 快速开始

![](https://snkkkait.oss-cn-beijing.aliyuncs.com/picgo/ruoyi-antdesign-4.png)

- 项目介绍
> 本项目为若依微服务RuoYi-Cloud版本Ant Design风格前端,使用如下命令即可快速开始，但前提是您已具备npm/nodejs环境
- 拉取代码
```jsx | pure
git clone https://gitee.com/xuezipeng/ruoyi-cloud-ant-design.git
```
- 安装 (如果安装较慢可以使用淘宝镜像)
```jsx | pure
npm install

(使用淘宝镜像安装)npm install --registry=https://registry.npm.taobao.org
```
- 启动
```jsx | pure
npm run serve
```
[项目地址](https://gitee.com/xuezipeng/ruoyi-cloud-ant-design)