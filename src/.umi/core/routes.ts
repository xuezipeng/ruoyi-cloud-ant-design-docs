// @ts-nocheck
import React from 'react';
import { ApplyPluginsType } from 'D:/myantd-docs/dumi-docs/node_modules/@umijs/runtime';
import * as umiExports from './umiExports';
import { plugin } from './plugin';

export function getRoutes() {
  const routes = [
  {
    "path": "/~demos/:uuid",
    "layout": false,
    "wrappers": [require('D:/myantd-docs/dumi-docs/node_modules/@umijs/preset-dumi/lib/theme/layout').default],
    "component": (props) => {
      const React = require('react');
      const renderArgs = require('../../../node_modules/@umijs/preset-dumi/lib/plugins/features/demo/getDemoRenderArgs').default(props);

      switch (renderArgs.length) {
        case 1:
          // render demo directly
          return renderArgs[0];

        case 2:
          // render demo with previewer
          return React.createElement(
            require('dumi-theme-default/src/builtins/Previewer.tsx').default,
            renderArgs[0],
            renderArgs[1],
          );

        default:
          return `Demo ${uuid} not found :(`;
      }
    }
  },
  {
    "path": "/_demos/:uuid",
    "redirect": "/~demos/:uuid"
  },
  {
    "__dumiRoot": true,
    "layout": false,
    "path": "/",
    "wrappers": [require('D:/myantd-docs/dumi-docs/node_modules/@umijs/preset-dumi/lib/theme/layout').default, require('D:/myantd-docs/dumi-docs/node_modules/dumi-theme-default/src/layout.tsx').default],
    "routes": [
      {
        "path": "/components/basic/recommend",
        "component": require('D:/myantd-docs/dumi-docs/src/Basic/recommend.md').default,
        "exact": true,
        "meta": {
          "filePath": "src/Basic/recommend.md",
          "updatedTime": 1614596650000,
          "componentName": "Basic",
          "group": {
            "title": "基础",
            "path": "/components/basic"
          },
          "nav": {
            "title": "文档列表",
            "path": "/components"
          },
          "slugs": [
            {
              "depth": 2,
              "value": "推荐",
              "heading": "推荐"
            }
          ],
          "title": "推荐"
        },
        "title": "推荐"
      },
      {
        "path": "/components/basic/relation",
        "component": require('D:/myantd-docs/dumi-docs/src/Basic/relation.md').default,
        "exact": true,
        "meta": {
          "filePath": "src/Basic/relation.md",
          "updatedTime": 1614596650000,
          "componentName": "Basic",
          "group": {
            "title": "基础",
            "path": "/components/basic"
          },
          "nav": {
            "title": "文档列表",
            "path": "/components"
          },
          "slugs": [
            {
              "depth": 2,
              "value": "联系",
              "heading": "联系"
            }
          ],
          "title": "联系"
        },
        "title": "联系"
      },
      {
        "path": "/components/basic/summarize",
        "component": require('D:/myantd-docs/dumi-docs/src/Basic/summarize.md').default,
        "exact": true,
        "meta": {
          "filePath": "src/Basic/summarize.md",
          "updatedTime": 1614596650000,
          "componentName": "Basic",
          "order": 1,
          "group": {
            "title": "基础",
            "path": "/components/basic"
          },
          "nav": {
            "title": "文档列表",
            "path": "/components"
          },
          "slugs": [
            {
              "depth": 2,
              "value": "简介",
              "heading": "简介"
            },
            {
              "depth": 1,
              "value": "RuoYi-Cloud-Antdv",
              "heading": "ruoyi-cloud-antdv"
            }
          ],
          "title": "简介"
        },
        "title": "简介"
      },
      {
        "path": "/components/basic/thanks",
        "component": require('D:/myantd-docs/dumi-docs/src/Basic/thanks.md').default,
        "exact": true,
        "meta": {
          "filePath": "src/Basic/thanks.md",
          "updatedTime": 1614596650000,
          "componentName": "Basic",
          "group": {
            "title": "基础",
            "path": "/components/basic"
          },
          "nav": {
            "title": "文档列表",
            "path": "/components"
          },
          "slugs": [
            {
              "depth": 2,
              "value": "致谢",
              "heading": "致谢"
            }
          ],
          "title": "致谢"
        },
        "title": "致谢"
      },
      {
        "path": "/components/builder",
        "component": require('D:/myantd-docs/dumi-docs/src/Builder/index.md').default,
        "exact": true,
        "meta": {
          "filePath": "src/Builder/index.md",
          "updatedTime": 1614596650000,
          "componentName": "Builder",
          "group": {
            "title": "构建",
            "path": "/components/builder"
          },
          "nav": {
            "title": "文档列表",
            "path": "/components"
          },
          "slugs": [
            {
              "depth": 2,
              "value": "搭建",
              "heading": "搭建"
            }
          ],
          "title": "搭建"
        },
        "title": "搭建"
      },
      {
        "path": "/components/show",
        "component": require('D:/myantd-docs/dumi-docs/src/Show/index.md').default,
        "exact": true,
        "meta": {
          "filePath": "src/Show/index.md",
          "updatedTime": 1614596650000,
          "componentName": "Show",
          "group": {
            "title": "展示图",
            "path": "/components/show"
          },
          "nav": {
            "title": "文档列表",
            "path": "/components"
          },
          "slugs": [
            {
              "depth": 2,
              "value": "展示图",
              "heading": "展示图"
            }
          ],
          "title": "展示图"
        },
        "title": "展示图"
      },
      {
        "path": "/components/start",
        "component": require('D:/myantd-docs/dumi-docs/src/Start/index.md').default,
        "exact": true,
        "meta": {
          "filePath": "src/Start/index.md",
          "updatedTime": 1614596650000,
          "componentName": "Start",
          "group": {
            "title": "快速开始",
            "path": "/components/start"
          },
          "nav": {
            "title": "文档列表",
            "path": "/components"
          },
          "slugs": [
            {
              "depth": 2,
              "value": "快速开始",
              "heading": "快速开始"
            }
          ],
          "title": "快速开始"
        },
        "title": "快速开始"
      },
      {
        "path": "/components/structure",
        "component": require('D:/myantd-docs/dumi-docs/src/Structure/index.md').default,
        "exact": true,
        "meta": {
          "filePath": "src/Structure/index.md",
          "updatedTime": 1614658278000,
          "componentName": "Structure",
          "group": {
            "title": "项目结构",
            "path": "/components/structure"
          },
          "nav": {
            "title": "文档列表",
            "path": "/components"
          },
          "slugs": [
            {
              "depth": 2,
              "value": "项目结构",
              "heading": "项目结构"
            }
          ],
          "title": "项目结构"
        },
        "title": "项目结构"
      },
      {
        "path": "/",
        "component": require('D:/myantd-docs/dumi-docs/docs/index.md').default,
        "exact": true,
        "meta": {
          "filePath": "docs/index.md",
          "updatedTime": 1614652509000,
          "title": "RuoYi-Cloud-AntdV - 更好看的前端",
          "order": 10,
          "hero": {
            "title": "RuoYi-Cloud-Ant-Design",
            "desc": "<div class=\"markdown\"><p>📖 更好看的若依微服务前端UI风格</p></div>",
            "actions": [
              {
                "text": "开始使用",
                "link": "/components/start"
              }
            ]
          },
          "features": [
            {
              "icon": "https://iconfont.alicdn.com/t/57005912-edbf-40b5-8c8d-38e345907eb3.png",
              "title": "免费开源",
              "desc": "<div class=\"markdown\"><p>本着开源的初衷，所有代码免费开源，可以随意进行二次开发，持续维护！</p></div>"
            },
            {
              "icon": "https://iconfont.alicdn.com/t/43a3956f-52c7-4170-97ba-c2575e096a7d.png",
              "title": "更好看的UI风格",
              "desc": "<div class=\"markdown\"><p>相比于传统的ElementUI，Ant Design的UI风格更加新颖</p></div>"
            },
            {
              "icon": "https://iconfont.alicdn.com/t/b065a4dc-4a9d-405d-b9b6-955d4f93c78e.png",
              "title": "兼容性",
              "desc": "<div class=\"markdown\"><p>完全与若依微服务RuoYi-Cloud相结合，无需修改后端任何代码，持续更新！</p></div>"
            }
          ],
          "footer": "<div class=\"markdown\"><p>Copyright © 2021 by <a href=\"https://gitee.com/xuezipeng\" target=\"_blank\">xuezipeng<svg xmlns=\"http://www.w3.org/2000/svg\" aria-hidden=\"\" x=\"0px\" y=\"0px\" viewBox=\"0 0 100 100\" width=\"15\" height=\"15\" class=\"__dumi-default-external-link-icon\"><path fill=\"currentColor\" d=\"M18.8,85.1h56l0,0c2.2,0,4-1.8,4-4v-32h-8v28h-48v-48h28v-8h-32l0,0c-2.2,0-4,1.8-4,4v56C14.8,83.3,16.6,85.1,18.8,85.1z\"></path><polygon fill=\"currentColor\" points=\"45.7,48.7 51.3,54.3 77.2,28.5 77.2,37.2 85.2,37.2 85.2,14.9 62.8,14.9 62.8,22.9 71.5,22.9\"></polygon></svg></a> All Rights Reserved.  <br />闽ICP备20002003号</p></div>",
          "slugs": []
        },
        "title": "RuoYi-Cloud-AntdV - 更好看的前端"
      },
      {
        "path": "/",
        "component": require('D:/myantd-docs/dumi-docs/docs/index.zh-CN.md').default,
        "exact": true,
        "meta": {
          "filePath": "docs/index.zh-CN.md",
          "updatedTime": 1614590110000,
          "title": "dumi - 为组件开发场景而生的文档工具",
          "order": 10,
          "hero": {
            "title": "dumi",
            "desc": "<div class=\"markdown\"><p>📖 为组件开发场景而生的文档工具</p></div>",
            "actions": [
              {
                "text": "开始使用",
                "link": "/components/start"
              }
            ]
          },
          "features": [
            {
              "icon": "https://gw.alipayobjects.com/zos/bmw-prod/881dc458-f20b-407b-947a-95104b5ec82b/k79dm8ih_w144_h144.png",
              "title": "开箱即用",
              "desc": "<div class=\"markdown\"><p>考究的默认配置和约定式的目录结构，帮助开发者零成本上手，让所有注意力都能放在文档编写和组件开发上</p></div>"
            },
            {
              "icon": "https://gw.alipayobjects.com/zos/bmw-prod/d1ee0c6f-5aed-4a45-a507-339a4bfe076c/k7bjsocq_w144_h144.png",
              "title": "为组件开发而生",
              "desc": "<div class=\"markdown\"><p>丰富的 Markdown 扩展，不止于渲染组件 demo，使得组件的文档不仅易于编写、管理，还好看、好用</p></div>"
            },
            {
              "icon": "https://gw.alipayobjects.com/zos/bmw-prod/b8570f4d-c1b1-45eb-a1da-abff53159967/kj9t990h_w144_h144.png",
              "title": "主题系统",
              "desc": "<div class=\"markdown\"><p>渐进式的自定义主题能力，小到扩展自己的 Markdown 标签，大到自定义完整主题包，全由你定</p></div>"
            },
            {
              "icon": "https://gw.alipayobjects.com/zos/bmw-prod/b3e102cd-5dad-4046-a02a-be33241d1cc7/kj9t8oji_w144_h144.png",
              "title": "API 自动生成",
              "desc": "<div class=\"markdown\"><p>可基于 TypeScript 类型定义自动生成组件 API，组件永远『表里如一』</p></div>"
            },
            {
              "icon": "https://gw.alipayobjects.com/zos/bmw-prod/3863e74a-7870-4874-b1e1-00a8cdf47684/kj9t7ww3_w144_h144.png",
              "title": "移动端组件库研发",
              "desc": "<div class=\"markdown\"><p>安装主题包即可快速启用移动端组件研发能力，内置移动端高清渲染方案</p></div>"
            },
            {
              "icon": "https://gw.alipayobjects.com/zos/bmw-prod/f093e060-726e-471c-a53e-e988ed3f560c/kj9t9sk7_w144_h144.png",
              "title": "资产数据化能力",
              "desc": "<div class=\"markdown\"><p>一行命令将组件资产数据化，标准化的资产数据可与下游生产力工具串联</p></div>"
            }
          ],
          "footer": "<div class=\"markdown\"><p>Open-source MIT Licensed | Copyright © 2019-present<br />Powered by self</p></div>",
          "slugs": [
            {
              "depth": 2,
              "value": "轻松上手",
              "heading": "轻松上手"
            },
            {
              "depth": 2,
              "value": "反馈与共建",
              "heading": "反馈与共建"
            }
          ],
          "locale": "zh-CN"
        },
        "title": "dumi - 为组件开发场景而生的文档工具"
      },
      {
        "path": "/components/basic",
        "meta": {},
        "exact": true,
        "redirect": "/components/basic/summarize"
      },
      {
        "path": "/components",
        "meta": {},
        "exact": true,
        "redirect": "/components/basic/summarize"
      }
    ],
    "title": "RuoYi-Cloud-Ant-Design",
    "component": (props) => props.children
  }
];

  // allow user to extend routes
  plugin.applyPlugins({
    key: 'patchRoutes',
    type: ApplyPluginsType.event,
    args: { routes },
  });

  return routes;
}
