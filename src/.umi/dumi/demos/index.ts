// @ts-nocheck
import React from 'react';
import { dynamic } from 'dumi';

export default {
  'Structure-demo': {
    component: () => React.createElement(dynamic({
      loader: async function() {
        const { default: demos } = await import(/* webpackChunkName: "demos_erutcurtS" */'./Structure');

        return demos['Structure-demo'].component;
      },
      loading: () => null,
    })),
    previewerProps: {"sources":{"_":{"jsx":"import React from 'react';\n\nexport default () => (\n  <div>\n    <ul>\n      <li>\n        src/\n        <ul>\n          <li>api(存放接口)</li>\n          <li>assets(存放一些静态资源，图片等)</li>\n          <li>components(存放组件,由Ant Design Vue Pro提供)</li>\n          <li>config(项目配置文件如全局样式、路由)</li>\n          <li>core(项目核心文件夹，用于配置一些必备的组件/图标)</li>\n          <li>\n            layouts(项目布局文件夹，路由以及固定的侧边栏所采用的组件都在这里哦)\n          </li>\n          <li>locales(国际化包)</li>\n          <li>mock(Ant Design Vue Pro前端模拟数据，现已不用！)</li>\n          <li>router(动态路由的实现)</li>\n          <li>store(Vuex.Store配置)</li>\n          <li>utils(工具包)</li>\n          <li>views(存放页面)</li>\n        </ul>\n      </li>\n    </ul>\n  </div>\n);"}},"dependencies":{"react":{"version":"16.14.0"}},"componentName":"Structure","transform":true,"identifier":"Structure-demo"},
  },
};
