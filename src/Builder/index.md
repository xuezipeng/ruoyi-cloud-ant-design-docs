---
group:
  title: 构建
nav:
  title: 文档列表
  path: /components
---

## 搭建

- 1.环境准备

请先确保主机已经安装git，nodejs，npm环境

- 2.拉取代码

```jsx | pure
git clone https://gitee.com/xuezipeng/ruoyi-cloud-ant-design.git
```

- 3.安装 (如果安装较慢可以使用淘宝镜像)

```jsx | pure
npm install

(使用淘宝镜像安装)npm install --registry=https://registry.npm.taobao.org
```

- 4.启动

```jsx | pure
npm run serve
```
structure
> 友情提示：本项目默认后端接口使用的是我们演示站的，记得改成自己的 在vue.config.js中修改~
