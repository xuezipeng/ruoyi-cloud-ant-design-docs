---
group:
  title: 项目结构
nav:
  title: 文档列表
  path: /components
---

## 项目结构

```jsx
/**
 * transform: true
 */

import React from 'react';

export default () => (
  <div>
    <ul>
      <li>
        src/
        <ul>
          <li>api(存放接口)</li>
          <li>assets(存放一些静态资源，图片等)</li>
          <li>components(存放组件,由Ant Design Vue Pro提供)</li>
          <li>config(项目配置文件如全局样式、路由)</li>
          <li>core(项目核心文件夹，用于配置一些必备的组件/图标)</li>
          <li>
            layouts(项目布局文件夹，路由以及固定的侧边栏所采用的组件都在这里哦)
          </li>
          <li>locales(国际化包)</li>
          <li>mock(Ant Design Vue Pro前端模拟数据，现已不用！)</li>
          <li>router(动态路由的实现)</li>
          <li>store(Vuex.Store配置)</li>
          <li>utils(工具包)</li>
          <li>views(存放页面)</li>
        </ul>
      </li>
    </ul>
  </div>
);
```
