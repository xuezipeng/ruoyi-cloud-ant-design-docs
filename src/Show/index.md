---
group:
  title: 展示图
nav:
  title: 文档列表
  path: /components
---

## 展示图

**演示站：http://182.61.136.176**

**演示账号：admin**

**演示密码：admin123**

**如果在演示站中发现 BUG/演示站宕机请联系 QQ：2531639937 修复~**

| ![img](https://snkkkait.oss-cn-beijing.aliyuncs.com/picgo/20210101113510.png)        | ![img](https://snkkkait.oss-cn-beijing.aliyuncs.com/picgo/20210101113541.png)        |
| ------------------------------------------------------------------------------------ | ------------------------------------------------------------------------------------ |
| ![img](https://snkkkait.oss-cn-beijing.aliyuncs.com/picgo/20210101113610.png)        | ![img](https://snkkkait.oss-cn-beijing.aliyuncs.com/picgo/20210101113628.png)        |
| ![img](https://snkkkait.oss-cn-beijing.aliyuncs.com/picgo/20210101113749.png)        | ![img](https://snkkkait.oss-cn-beijing.aliyuncs.com/picgo/20210101113826.png)        |
| ![img](https://snkkkait.oss-cn-beijing.aliyuncs.com/picgo/20210101113857.png)        | ![img](https://snkkkait.oss-cn-beijing.aliyuncs.com/picgo/20210101113922.png)        |
| ![img](https://snkkkait.oss-cn-beijing.aliyuncs.com/picgo/20210101113945.png)        | ![img](https://snkkkait.oss-cn-beijing.aliyuncs.com/picgo/20210101114121.png)        |
| ![img](https://snkkkait.oss-cn-beijing.aliyuncs.com/picgo/20210101114237.png)        | ![img](https://snkkkait.oss-cn-beijing.aliyuncs.com/picgo/20210101114256.png)        |
| !![img](https://snkkkait.oss-cn-beijing.aliyuncs.com/picgo/20210101114540.png)       | ![img](https://snkkkait.oss-cn-beijing.aliyuncs.com/picgo/20210101114602.png)        |
| ![img](https://oscimg.oschina.net/oscnet/up-ece3fd37a3d4bb75a3926e905a3c5629055.png) | ![img](https://oscimg.oschina.net/oscnet/up-92ffb7f3835855cff100fa0f754a6be0d99.png) |
| ![img](https://oscimg.oschina.net/oscnet/up-ff9e3066561574aca73005c5730c6a41f15.png) |                                                                                      |
